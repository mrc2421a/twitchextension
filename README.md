# TwitchExtension
Extension chrome/firefox redirigeant vers un stream.
Indique par son icone si le stream est online ou offline

Pour l'installation :
Télécharger l'archive (.zip)
Extraire le dossier ("extraire ici")
Sur chrome :
    -> Paramètres 
    -> Extensions 
    -> Mode développeur 
    -> Chargez l'extension non empaquetée
    -> Sélectionner le dossier
    
Bon live !