chrome.browserAction.onClicked.addListener(function openInNewTab(url) {
  var win = window.open("https://www.twitch.tv/professor_vayne", '_blank');
  win.focus();
});

// const bkg = chrome.extension.getBackgroundPage();

var type = 0;
updateStreamState();
setInterval(updateStreamState, 2000);
function updateStreamState(){
  $.ajax({
    headers: {'Client-ID': 'g5c18qjb7r27bqq0wjo4yudwdnvbgt'},
    url: "https://api.twitch.tv/helix/streams?user_login=professor_vayne",
    dataType: 'json',
    success: function(data){
      // bkg.console.log(data)

      if(data["data"].length != 0){
        if (data["data"][0]["type"] == "live") {chrome.browserAction.setIcon({path:"img/vayne_on.png"})}
      } else {
        if(type == 0){
          type = 1;
          chrome.browserAction.setIcon({path:"img/vayne_off.png"});
        } else {
          type = 0;
          chrome.browserAction.setIcon({path:"img/vayne_neutral.png"});
        }
      }
    }
  });
}
